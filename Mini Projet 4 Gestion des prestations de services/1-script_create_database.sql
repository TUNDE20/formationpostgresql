-- Suppression de la base de données si elle existe déjà
DROP DATABASE IF EXISTS "IPRESTATIONS_SERVICES";

-- Création de la base de données "I_PRESTATIONS_SERVICES"
CREATE DATABASE "IPRESTATIONS_SERVICES";

