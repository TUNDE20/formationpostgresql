CREATE TABLE "TB_CLIENT"
(
"ID_CLIENT" VARCHAR(10),
"NOM_CLIENT" VARCHAR(50),
"NUM_TEL_CLIENT" VARCHAR(20)
)

INSERT INTO "TB_CLIENT"
VALUES 
	('001', 'Raoul', ''),
	('002', 'Anne', ''),
	('003', 'Martinien', '0000000000'),
	('004', 'Anaïs', null),
	('005', 'Léo', '0000000001'),
	('006', 'Léa', '0000000002'),
	('007', 'Martin', null),
	('008', 'Martine', null);

SELECT *
FROM "TB_CLIENT";

--- Afficher le nombre de client dont le numéro de téléphone est différent de '0000000000'
SELECT COUNT(1) AS "Nb Clients"
FROM "TB_CLIENT"
WHERE "NUM_TEL_CLIENT" <> '0000000000';